
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include "atom.h"
#include "composition.h"
using namespace std;

int main(int argc, char** argv) {

    if (argc != 2) {
        cout << "Invalid syntax\n";
    }

    istringstream ss(argv[1]); //elegxei an to orisma argv[1] einai akeraios
    int x;
    if (!(ss >> x)) {
        cerr << "Invalid number " << argv[1] << '\n';
    }

    srand(time(NULL));

    for (int i = 0; i < x; i++) {

        atom** carbon_array;
        atom** hydrogen_array;



        int rand_no_carbon_atoms = rand() % (10 - 1 + 1) + 1;
        cout << "\n" << rand_no_carbon_atoms << " Random carbon atoms will be created " << endl;
        carbon_array = new atom*[rand_no_carbon_atoms];

        for (int i = 0; i < rand_no_carbon_atoms; i++) {
            string c = "c";
            char result[20]; // string which will contain the number
            sprintf(result, "%d", i); //Ftiaxnetai to onoma tou ka8e an8raka
            string s(result);
            string name = c + s;
            carbon_array[i] = new atom(4, name, "carbon");
        }

        cout << "\n\n";

        int no_hydrogen_atoms;

        int rand_hydrocarbon = rand() % (3 - 1 + 1) + 1;

        if (rand_no_carbon_atoms == 1) {
            rand_hydrocarbon = 1;
        }

        if (rand_hydrocarbon == 1) {
            no_hydrogen_atoms = 2 * rand_no_carbon_atoms + 2;
            cout << "\nAlkane comp\n\n";
        }

        if (rand_hydrocarbon == 2) {
            no_hydrogen_atoms = 2 * rand_no_carbon_atoms;
            cout << "\nAlkene comp\n\n";
        }

        if (rand_hydrocarbon == 3) {
            no_hydrogen_atoms = 2 * rand_no_carbon_atoms - 2;
            cout << "\nAlkyne comp\n\n";
        }


        cout << no_hydrogen_atoms << " Random hydrogen atoms will be created " << endl;
        hydrogen_array = new atom*[no_hydrogen_atoms];

        for (int y = 0; y < no_hydrogen_atoms; y++) {
            string h = "h";
            char result[20]; // string which will contain the number
            sprintf(result, "%d", y); //Ftiaxnetai to onoma tou ka8e udrogonou
            string s(result);
            string name = h + s;
            hydrogen_array[y] = new atom(1, name, "hydrogen");
        }

        bond** bond_array;
        int no_bonds;
        int index = 0;

        if (rand_no_carbon_atoms == 1) {

            carbon_array[0]->set_valence_electrons(8);
            for (int i = 0; i < 4; i++) {
                hydrogen_array[i]->set_valence_electrons(2);
            }
            no_bonds = 4;
            bond_array = new bond*[no_bonds];
            for (int i = 0; i < no_bonds; i++) {
                bond_array[i] = new bond("single");
                bond_array[i]->set_atom_array(carbon_array[0], hydrogen_array[i]);
                carbon_array[0]->set_bond(bond_array[i], no_bonds, i);
                hydrogen_array[i]->set_bond(bond_array[i], 1, 0);
            }
        } else {
            no_bonds = rand_no_carbon_atoms + no_hydrogen_atoms - 1;
            bond_array = new bond*[no_bonds];
            int bond_counter = 0;
            int index = 0;
            for (int i = 0; i < rand_no_carbon_atoms; i++) {
                int b_index = 0;
                if (i == 0) { // Briskomai ston prwto an8raka
                    carbon_array[i]->set_valence_electrons(1);
                    bond_array[bond_counter] = new bond("single");
                    bond_array[bond_counter]->set_atom_array(carbon_array[i], carbon_array[i + 1]);
                    carbon_array[i]->set_bond(bond_array[bond_counter], 4, b_index);
                    carbon_array[i + 1]->set_bond(bond_array[bond_counter], 4, b_index);
                    bond_counter++;
                    b_index++;
                } else if (i == rand_no_carbon_atoms - 1) { //Briskomai ston teleutaio an8raka
                    carbon_array[i]->set_valence_electrons(1);
                    b_index++;
                } else { //Briskomai se endiamesous an8rakes
                    carbon_array[i]->set_valence_electrons(2);
                    bond_array[bond_counter] = new bond("single");
                    bond_array[bond_counter]->set_atom_array(carbon_array[i], carbon_array[i + 1]);
                    carbon_array[i]->set_bond(bond_array[bond_counter], 4, b_index);
                    carbon_array[i + 1]->set_bond(bond_array[bond_counter], 4, b_index);
                    bond_counter++;
                    b_index++;
                }
                for (int y = index; y < no_hydrogen_atoms; y++) { //Edw sumplhrwnw udrogona
                    if (carbon_array[i]->get_valence_electrons() != 8 && hydrogen_array[y]->get_valence_electrons() != 2) {
                        carbon_array[i]->set_valence_electrons(1);
                        hydrogen_array[y]->set_valence_electrons(1);
                        bond_array[bond_counter] = new bond("single");
                        bond_array[bond_counter]->set_atom_array(carbon_array[i], hydrogen_array[y]);
                        carbon_array[i]->set_bond(bond_array[bond_counter], 4, b_index);
                        hydrogen_array[y]->set_bond(bond_array[bond_counter], 1, 0);
                        bond_counter++;
                        b_index++;
                    } else {
                        index = y;
                        break;
                    }
                }
            }
        }


        int no_double_bonds = 0;
        int no_triple_bonds = 0;


        for (int i = 0; i < rand_no_carbon_atoms; i++) {
            if (carbon_array[i]->get_valence_electrons() == 5) {
                if (rand_no_carbon_atoms == 2) {
                    carbon_array[i]->set_valence_electrons(3);
                    bond_array[0]->set_type("triple");
                    bond_array[2]->set_atom_array(carbon_array[i], hydrogen_array[1]);
                    carbon_array[i]->set_bond(bond_array[0], 4, 0);
                    carbon_array[i]->set_bond(bond_array[2], 4, 1);
                    carbon_array[i]->delete_bond(2);
                    carbon_array[i]->delete_bond(3);
                    carbon_array[0]->delete_bond(3);
                    hydrogen_array[1]->set_bond(bond_array[2], 1, 0);
                    no_triple_bonds++;
                } else {
                    carbon_array[i]->set_valence_electrons(3);
                    bond_array[no_bonds - 2]->set_type("triple");
                    bond_array[no_bonds - 1]->set_atom_array(carbon_array[i], hydrogen_array[no_hydrogen_atoms - 1]);
                    carbon_array[i]->set_bond(bond_array[no_bonds - 1], 4, 1);
                    carbon_array[i]->delete_bond(2);
                    carbon_array[i]->delete_bond(3);
                    carbon_array[i - 1]->delete_bond(3);
                    hydrogen_array[no_hydrogen_atoms - 1]->set_bond(bond_array[no_bonds - 2], 1, 0);
                    no_triple_bonds++;
                }
            }
            if (carbon_array[i]->get_valence_electrons() == 6) {
                if (rand_no_carbon_atoms == 2) {
                    carbon_array[i]->set_valence_electrons(2);
                    bond_array[0]->set_type("double");
                    bond_array[no_bonds - 2]->set_atom_array(carbon_array[i], hydrogen_array[no_hydrogen_atoms - 2]);
                    carbon_array[i]->set_bond(bond_array[no_bonds - 2], 4, 2);
                    carbon_array[i]->delete_bond(3);
                    hydrogen_array[no_hydrogen_atoms - 2]->set_bond(bond_array[no_bonds - 2], 1, 0);
                    no_double_bonds++;
                } else {
                    carbon_array[i]->set_valence_electrons(2);
                    bond_array[no_bonds - 4]->set_type("double");
                    bond_array[no_bonds - 2]->set_atom_array(carbon_array[i], hydrogen_array[no_hydrogen_atoms - 2]);
                    carbon_array[i]->set_bond(bond_array[no_bonds - 4], 4, 0);
                    carbon_array[i - 1]->set_bond(bond_array[no_bonds - 4], 4, 0);
                    carbon_array[i]->set_bond(bond_array[no_bonds - 2], 4, 2);
                    carbon_array[i]->delete_bond(3);
                    hydrogen_array[no_hydrogen_atoms - 2]->set_bond(bond_array[no_bonds - 2], 1, 0);
                    no_double_bonds++;
                }
            }
            if (carbon_array[i]->get_valence_electrons() == 7) {
                carbon_array[i]->set_valence_electrons(1);
            }
        }


        cout << "\n\n";

        for (int i = 0; i < rand_no_carbon_atoms; i++) {
            carbon_array[i]->print();
        }

        cout << "\n\n";

        cout << "Double bonds: " << no_double_bonds << "\n";
        cout << "Triple bonds: " << no_triple_bonds << "\n";

        cout << "\n\n";

        for (int i = 0; i < no_hydrogen_atoms; i++) {
            hydrogen_array[i]->print();
        }

        cout << "\n\n";

        string c = "C";
        char result[20]; // string which will contain the number
        sprintf(result, "%d", rand_no_carbon_atoms);
        string s(result);
        string name = c + s;
        sprintf(result, "%d", no_hydrogen_atoms);
        string s1(result);
        name = name + "H" + s1;

        composition composition1(no_bonds, name);
        composition1.set_bonds(bond_array);
        composition1.print();

        cout << "\n\n";

        for (int i = 0; i < rand_no_carbon_atoms; i++) {
            delete carbon_array[i];
        }

        delete[] carbon_array;

        for (int i = 0; i < no_hydrogen_atoms; i++) {
            delete hydrogen_array[i];
        }

        delete[] hydrogen_array;

        for (int i = 0; i < no_bonds; i++) {
            delete bond_array[i];
        }

        delete[] bond_array;

    }

    return 0;
}
