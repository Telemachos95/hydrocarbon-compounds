
#ifndef COMPOSITION
#define COMPOSITION

#include <string>
#include "bond.h"
using namespace std;

class composition {
private:
    bond** bonds;
    string category; // to xrhsimopoiw gia ton suntaktiko tupo ths enwshs
    int bonds_number;
public:
    composition(int no_bonds, string comp_category);
    ~composition();
    void set_bonds(bond** comp_bonds);
    void print();
};

#endif