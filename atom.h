
#ifndef ATOM
#define ATOM

#include <string>
#include "bond.h"
using namespace std;

class bond;

class atom {
private:
    bond** bonds;
    string name;
    string type;
    int valence_electrons;
public:
    atom();
    atom(int val_electrons, string atom_name, string atom_type);
    ~atom();
    void set_bond(bond* bond, int no_bonds, int counter);
    string get_name();
    void set_valence_electrons(int addition);
    int get_valence_electrons();
    void delete_bond(int index);
    void print();
};

#endif