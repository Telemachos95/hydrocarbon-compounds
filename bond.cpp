
#include <iostream>
#include <string>
#include "atom.h"
#include "bond.h"
using namespace std;

bond::bond() {
    atom_array = new atom*[2];
    for (int i = 0; i < 2; i++) {
        atom_array[i] = new atom();
    }
}

bond::bond(string bond_type) {
    atom_array = new atom*[2];
    for (int i = 0; i < 2; i++) {
        atom_array[i] = new atom();
    }
    type = bond_type;
}

bond::~bond() {
    cout << "bond: " << type << " destroyed\n";
    delete[] atom_array;
}

void bond::set_atom_array(atom* atom1, atom* atom2) {
    atom_array[0] = atom1;
    atom_array[1] = atom2;
}

atom** bond::get_atom_array() {
    return atom_array;
}

void bond::set_type(string bond_type) {
    type = bond_type;
}

string bond::get_type() {
    return type;
}