
#ifndef BOND
#define BOND

#include <string>
#include "atom.h"
using namespace std;

class atom;

class bond {
private:
    atom** atom_array;
    string type;
public:
    bond();
    bond(string bond_type);
    ~bond();
    void set_atom_array(atom* atom1, atom* atom2);
    atom** get_atom_array();
    void set_type(string bond_type);
    string get_type();
};

#endif