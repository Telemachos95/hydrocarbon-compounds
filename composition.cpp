
#include <iostream>
#include <string>
#include "composition.h"
using namespace std;

composition::composition(int no_bonds, string comp_category) {
    bonds = new bond*[no_bonds];
    category = comp_category;
    bonds_number = no_bonds;
}

composition::~composition() {
    cout << "comp: " << category << " destroyed \n";
}

void composition::set_bonds(bond** comp_bonds) {
    for (int i = 0; i < bonds_number; i++) {
        bonds[i] = new bond();
    }
    bonds = comp_bonds;

}

void composition::print() {
    cout << "\n" << category << "\n";
    cout << "comp([";
    for (int i = 0; i < bonds_number; i++) {
        atom** arr = new atom*[2];
        for (int x = 0; x < 2; x++) {
            arr[x] = new atom();
        }
        arr = bonds[i]->get_atom_array();


        string s1 = arr[0]->get_name();
        string s2 = arr[1]->get_name();
        cout << s1 << "-" << bonds[i]->get_type() << "-" << s2;
        if (i != bonds_number - 1) {
            cout << ",";
        }

    }
    cout << "])\n" << "\n";

}