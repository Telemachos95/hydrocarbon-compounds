
compositions: main.o atom.o bond.o composition.o
	g++ -o compositions main.o atom.o bond.o composition.o -g
	
main.o: main.cpp 
	g++ -o main.o -c main.cpp
	
bond.o: bond.cpp 
	g++ -o bond.o -c bond.cpp

atom.o: atom.cpp atom.h
	g++ -o atom.o -c atom.cpp
 


composition.o: composition.cpp composition.h bond.h
	g++ -o composition.o -c composition.cpp


