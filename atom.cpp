
#include <iostream>
#include <cstring>
#include "atom.h"
using namespace std;

atom::atom() {

}

atom::atom(int val_electrons, string atom_name, string atom_type) {
    bonds = new bond*[val_electrons];
    for (int i = 0; i < val_electrons; i++) {
        bonds[i] = new bond();
    }
    name = atom_name;
    type = atom_type;
    valence_electrons = val_electrons;
    cout << "Atom " << name << " created" << "\n";
}

atom::~atom() {
    cout << "Atom " << name << " destroyed" << "\n";
    delete[] bonds;
}

void atom::set_bond(bond* bond, int no_bonds, int b_index) {
    if (no_bonds == 1) {
        bonds[0] = bond;
    } else {
        bonds[b_index] = bond;
    }
}

string atom::get_name() {
    return name;
}

void atom::set_valence_electrons(int addition) { //xrhsimopoieitai gia na pros8esei hlektronia s8enous sta hdh uparxonta.
    valence_electrons += addition;
}

int atom::get_valence_electrons() {
    return valence_electrons;
}

void atom::delete_bond(int index) {
    delete bonds[index];
}

void atom::print() {
    cout << "Atom: " << " name: " << name << " type: " << type << " valence_electrons: " << valence_electrons << "\n";
}